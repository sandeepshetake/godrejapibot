﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;

namespace GodrejAPIBot.Dialogs
{
    [Serializable]
    public class GodrejDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {

            context.Wait(MessageReceivedAsyncStart);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsyncStart(IDialogContext context, IAwaitable<object> result)
        {

            await context.PostAsync($"WellCome to VRM System");
            await context.PostAsync($"For Vendor Details Please Enter Vendor Code");
            context.Wait(MessageReceivedAsync);
        }
        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            

            var activity = await result as Activity;

            // User message
            string userMessage = activity.Text;
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //Assuming that the api takes the user message as a query paramater
                   // string RequestURI = "https://samples.openweathermap.org/data/2.5/weather?q=" + userMessage+ "&appid=b6907d289e10d714a6e88b30761fae22";
                    string RequestURI = "http://localhost:59154/api/Vendor?VENDOR_CODE=" + userMessage ;
                    HttpResponseMessage responsemMsg = await client.GetAsync(RequestURI);
                    if (responsemMsg.IsSuccessStatusCode)
                    {
                        var apiResponse = await responsemMsg.Content.ReadAsStringAsync();
                        Vendor_Creation obj = JsonConvert.DeserializeObject<Vendor_Creation>(apiResponse);
                        if (apiResponse.Contains("null"))
                        {
                            await context.PostAsync($"Vendor Details Not Found");
                        }
                        else
                        {
                            //Post the API response to bot again
                            await context.PostAsync($"Vendor Details");
                            await context.PostAsync($"REQUEST NO:- {obj.REQUEST_NO}");
                            await context.PostAsync($"VENDOR CODE:- {obj.VENDOR_CODE}");
                            await context.PostAsync($"VENDOR NAME:- {obj.VENDOR_NAME}");
                            await context.PostAsync($"STATUS:- {obj.STATUS}");
                            await context.PostAsync($"PROCESS STATUS:- {obj.PROCESS_STATUS}");

                            //String[] apiResponseSplit = apiResponse.Split(',');
                            //foreach (String vendorDetails in apiResponseSplit)
                            //{
                            //    await context.PostAsync($"{vendorDetails}");
                            //}
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            context.Wait(MessageReceivedAsyncStart);
        }
    }
    public class Vendor_Creation
    {
        public string REQUEST_NO { get; set; }
        public string VENDOR_CODE { get; set; }
        public string VENDOR_NAME { get; set; }
        public string STATUS { get; set; }
        public string PROCESS_STATUS { get; set; }
    }
}